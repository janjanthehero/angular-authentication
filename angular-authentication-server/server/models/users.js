// Dependencies
const mongoose = require('mongoose');

// Get an instance of the mongoose schema
const Schema = mongoose.Schema;

// Create a new schema for user data in MongoDB
const userSchema = new Schema({
    email: String,
    password: String
})
// Create model from the schema and export it.
module.exports = mongoose.model('user', userSchema, 'users');