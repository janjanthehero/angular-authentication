export const environment = {
  production: true,
  apiURL: 'https://angular-authentication-server.herokuapp.com/api'
};