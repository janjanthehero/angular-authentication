import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../environments/environment';

// Production and Dev Api URLs are stored in the environment variables.
// Depending which enviroment is run (dev or production) a different URL will be used.
const API_URL = environment.apiURL;

@Injectable({
  providedIn: 'root'
})
export class EventService {


  private _eventsUrl = API_URL + '/events';
  private _specialEventsUrl = API_URL + '/specialEvents';

  constructor(private http: HttpClient) { }

  getEvents() {
    return this.http.get<any>(this._eventsUrl);
  }

  getSpecialEvents() {
    return this.http.get<any>(this._specialEventsUrl);
  }

}
