import { Component, OnInit } from '@angular/core';
import { EventService } from '../event.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  events = [];

  // Inject EventService
  constructor(private _eventService: EventService) { }

  ngOnInit() {
    // Subscribe to changes in the previously defined 'events' list
    this._eventService.getEvents()
      .subscribe(
        res => this.events = res,
        err => console.log(err) )
  }

  // "'Buy Tickets' button click handler"
  msg:String;
  eventClick() {
    this.msg='You can\'t actually buy any tickets. This is just play pretend.';
    return this.msg;
  }

}
