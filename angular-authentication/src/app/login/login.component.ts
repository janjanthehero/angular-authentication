import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // Object property for holding user login data
  loginUserData = {
    email: String,
    password: String
  };

  // We inject the AuthService into the constructer to be able to use it in this component class
  constructor(private _auth: AuthService, private _router: Router) { }

  ngOnInit(): void {
  }

  // Gets triggered from the template file via click handler
  loginUser(email, pass){
    this.loginUserData.email = email;
    this.loginUserData.password = pass;

    // Send the user data to the service and subscribe to get the response from the server
    // Response is either error message or success response message
    this._auth.loginUser(this.loginUserData).subscribe(
      (res:any) => {
        console.log(res)
        localStorage.setItem('token', res.token)
        this._router.navigate(['/specialEvents'])
      },
      err => (console.log(err))
    )
  }

}
