import { Component, OnInit } from '@angular/core';
import { EventService } from '../event.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-special-events',
  templateUrl: './special-events.component.html',
  styleUrls: ['./special-events.component.css']
})
export class SpecialEventsComponent implements OnInit {

  specialEvents = [];
  // Inject EventService
  constructor(private _eventService: EventService, private _router: Router) { }

  ngOnInit() {
    // Subscribe to changes in the 'specialEvents' list
    this._eventService.getSpecialEvents()
      .subscribe(
        res => this.specialEvents = res,
        err => { if (err instanceof HttpErrorResponse) {
          if(err.status == 401) {
            this._router.navigate(['/login']);
          }
        }
      }
    )
  }

  // "'Buy Tickets' button click handler"
  msg:String;
  eventClick() {
    this.msg='You can\'t actually buy any tickets. This is just play pretend.';
    return this.msg;
  }

}
